export interface List {
  title: string
  id: number
  configName: string
  configValue: string
  configType: string
}
