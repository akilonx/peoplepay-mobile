import React, { useState, useEffect } from 'react'
import {
  Keyboard,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { AppText } from './AppText'
import { ButtonGroup } from 'react-native-elements'

interface Props {
  newItemType: string
  newItemName: string // Prop that the TextInput is controlled by
  placeholderText: string
  createButtonText: string
  buttonTestId?: string
  textInputTestId?: string
  handleNameChange(title: string): void
  handleCreateNewItem(title: string): Promise<void>
}

export const NewItem: React.FunctionComponent<Props> = function(props) {
  const {
    newItemType,
    newItemName,
    placeholderText,
    createButtonText,
    handleNameChange,
    handleCreateNewItem
  } = props

  const [selected, setSelected] = useState(0)

  useEffect(() => {
    if (newItemType == 'radiobox') {
      if (newItemName == 'Traditional') {
        setSelected(0)
      } else {
        setSelected(1)
      }
    }
  }, [newItemName])

  const createNewItem = () => {
    if (newItemName !== '') {
      handleCreateNewItem(newItemName).then(() => {
        // Reset the text input
        handleNameChange('')
        // Dismiss keyboard
        Keyboard.dismiss()
      })
    }
  }

  const updateIndex = (value: number) => {
    setSelected(value)
    handleNameChange(buttons[value])

    handleCreateNewItem(buttons[value]).then(() => {
      handleNameChange('')
      Keyboard.dismiss()
    })
  }

  const buttons = ['Traditional', 'SSO']

  return (
    <View style={styles.wrapper}>
      {newItemType == 'radiobox' && (
        <ButtonGroup
          onPress={updateIndex}
          selectedIndex={selected}
          buttons={buttons}
          containerStyle={{ height: 50, width: '100%' }}
        />
      )}
      {newItemType == 'textbox' && (
        <>
          <TextInput
            placeholder={placeholderText}
            placeholderTextColor="#999"
            onChangeText={handleNameChange}
            value={newItemName}
            style={styles.textInput}
            testID={props.textInputTestId || 'newItemTextInput'}
            onSubmitEditing={createNewItem}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={createNewItem}
            testID={props.buttonTestId || 'newItemButton'}
          >
            <AppText>{createButtonText}</AppText>
          </TouchableOpacity>
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  textInput: {
    color: 'black',
    borderWidth: 1,
    padding: 5,
    flex: 4
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    paddingRight: 20
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 5
  }
})
