import React, { useEffect, useState } from 'react'
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Alert
} from 'react-native'
import { WebView } from 'react-native-webview'
import { LoadingScreen } from './LoadingScreen'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '../App'
import { useLists } from '../hooks/useLists'
import { List } from '../types/List'
import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/Ionicons'

interface Props {
  back(): void
  navigation: StackNavigationProp<RootStackParamList, 'Settings'>
}

export const WebviewScreen: React.FunctionComponent<Props> = function({
  navigation
}) {
  const [isDownloading, setIsDownloading] = useState(false)
  const { lists, createList } = useLists()
  const [url, setUrl] = useState<List>()
  const { navigate } = navigation
  const [webviewRef, setWebviewRef] = useState<any>()

  useEffect(() => {
    if (lists && lists.length > 0)
      setUrl(lists.find(a => a.configName == 'url'))
  }, [lists])

  return !url ? (
    <LoadingScreen text="Loading..." />
  ) : (
    <SafeAreaView style={styles.container} testID="webviewScreen">
      <WebView
        source={{
          uri: `${url.configValue}`
        }}
        javaScriptEnabledAndroid={true}
        javaScriptEnabled={true}
        onMessage={event => {
          //console.log('MESSAGE >>>>' + event.nativeEvent.data)
          //navigate('PeoplePayPdf', { name: event.nativeEvent.data })
        }}
        ref={ref => {
          setWebviewRef(ref)
        }}
      />
      <ActionButton
        renderIcon={() => (
          <Icon name="settings-outline" style={styles.actionButtonIcon} />
        )}
        buttonColor="rgba(89,133,195,1)"
        onPress={() => navigate('Settings')}
      ></ActionButton>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
    backgroundColor: '#f3f3f3'
  },
  dropboxButton: {
    alignItems: 'center',
    margin: 10,
    marginTop: 25,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 1,
    borderRadius: 3
  },
  actionButtonIcon: {
    fontSize: 40,
    color: 'white'
  }
})
