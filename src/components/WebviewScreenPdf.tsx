import React, { useEffect, useState } from 'react'
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Alert
} from 'react-native'
import { WebView } from 'react-native-webview'
import { LoadingScreen } from './LoadingScreen'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '../App'
import { useLists } from '../hooks/useLists'
import { List } from '../types/List'
import ActionButton from 'react-native-action-button'
import Icon from 'react-native-vector-icons/Ionicons'
import { RouteProp } from '@react-navigation/native'
import Pdf from 'react-native-pdf'

interface Props {
  back(): void
  name: string
  navigation: StackNavigationProp<RootStackParamList, 'PeoplePayPdf'>
  route: RouteProp<RootStackParamList, 'PeoplePayPdf'>
}

export const WebviewScreenPdf: React.FunctionComponent<Props> = function({
  navigation,
  route
}) {
  const [isDownloading, setIsDownloading] = useState(false)
  const { lists, createList } = useLists()
  const [url, setUrl] = useState<List>()
  const { navigate } = navigation

  //'http://samples.leanpub.com/thereactnativebook-sample.pdf'
  const source = {
    uri: `${route.params.name}`,
    cache: false
  }

  return (
    <View style={{ width: '100%', height: '100%' }}>
      <Pdf source={source} style={styles.pdf} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
    backgroundColor: '#ff0000'
  },
  dropboxButton: {
    alignItems: 'center',
    margin: 10,
    marginTop: 25,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 1,
    borderRadius: 3
  },
  actionButtonIcon: {
    fontSize: 30,
    height: 32,
    color: 'white'
  },
  pdf: {
    flex: 1
  }
})
