import { StyleSheet } from 'react-native'

export const sharedStyle = StyleSheet.create({
  headerWithButton: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  headerButton: {
    justifyContent: 'center',
    padding: 5
  }
})
